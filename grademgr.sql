-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 30-Mar-2019 às 00:15
-- Versão do servidor: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `grademgr`
--
CREATE DATABASE IF NOT EXISTS `grademgr` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `grademgr`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `curso`
--

DROP TABLE IF EXISTS `curso`;
CREATE TABLE IF NOT EXISTS `curso` (
  `CUR_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CUR_DESC` varchar(100) COLLATE utf8_bin NOT NULL,
  `CUR_INSTITUICAO` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `CUR_PERIODO_PADRAO` int(11) NOT NULL,
  PRIMARY KEY (`CUR_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `curso_disciplina_periodo`
--

DROP TABLE IF EXISTS `curso_disciplina_periodo`;
CREATE TABLE IF NOT EXISTS `curso_disciplina_periodo` (
  `CDP_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CUR_ID` int(11) NOT NULL,
  `DIS_ID` int(11) NOT NULL,
  `PEM_ID` int(11) NOT NULL,
  `CDP_DATA_INICIO` date NOT NULL,
  PRIMARY KEY (`CUR_ID`,`DIS_ID`,`PEM_ID`),
  KEY `DIS_ID` (`DIS_ID`),
  KEY `PEM_ID` (`PEM_ID`),
  KEY `CDP_ID` (`CDP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `disciplina`
--

DROP TABLE IF EXISTS `disciplina`;
CREATE TABLE IF NOT EXISTS `disciplina` (
  `DIS_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DIS_DESC` varchar(100) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`DIS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `nota`
--

DROP TABLE IF EXISTS `nota`;
CREATE TABLE IF NOT EXISTS `nota` (
  `NTA_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NTG_ID` int(11) NOT NULL,
  `NTA_VALOR` decimal(8,3) NOT NULL,
  `NTA_DESC` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `NTA_DATA` date DEFAULT NULL,
  PRIMARY KEY (`NTA_ID`),
  KEY `NTG_ID` (`NTG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `nota_grupo`
--

DROP TABLE IF EXISTS `nota_grupo`;
CREATE TABLE IF NOT EXISTS `nota_grupo` (
  `NTG_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CDP_ID` int(11) NOT NULL,
  `NTG_PESO` decimal(5,2) NOT NULL DEFAULT '100.00',
  `NTG_MEDIA_ARITMETICA` decimal(8,3) DEFAULT '0.000',
  PRIMARY KEY (`NTG_ID`),
  KEY `CDP_ID` (`CDP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `periodo_letivo`
--

DROP TABLE IF EXISTS `periodo_letivo`;
CREATE TABLE IF NOT EXISTS `periodo_letivo` (
  `PEL_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PEL_DATA_INICIO` date NOT NULL,
  `PEL_DATA_FIM` date DEFAULT NULL,
  PRIMARY KEY (`PEL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `periodo_meses`
--

DROP TABLE IF EXISTS `periodo_meses`;
CREATE TABLE IF NOT EXISTS `periodo_meses` (
  `PEM_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PEM_DESC` varchar(20) COLLATE utf8_bin NOT NULL,
  `PEM_MESES` int(2) NOT NULL,
  PRIMARY KEY (`PEM_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Extraindo dados da tabela `periodo_meses`
--

INSERT INTO `periodo_meses` (`PEM_ID`, `PEM_DESC`, `PEM_MESES`) VALUES
(1, 'Mensal', 1),
(2, 'Bimestral', 2),
(3, 'Trimestral', 3),
(4, 'Semestral', 6);

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `curso_disciplina_periodo`
--
ALTER TABLE `curso_disciplina_periodo`
  ADD CONSTRAINT `curso_disciplina_periodo_ibfk_1` FOREIGN KEY (`CUR_ID`) REFERENCES `curso` (`CUR_ID`),
  ADD CONSTRAINT `curso_disciplina_periodo_ibfk_2` FOREIGN KEY (`DIS_ID`) REFERENCES `disciplina` (`DIS_ID`),
  ADD CONSTRAINT `curso_disciplina_periodo_ibfk_3` FOREIGN KEY (`PEM_ID`) REFERENCES `periodo_meses` (`PEM_ID`);

--
-- Limitadores para a tabela `nota`
--
ALTER TABLE `nota`
  ADD CONSTRAINT `nota_ibfk_1` FOREIGN KEY (`NTG_ID`) REFERENCES `nota_grupo` (`NTG_ID`);

--
-- Limitadores para a tabela `nota_grupo`
--
ALTER TABLE `nota_grupo`
  ADD CONSTRAINT `nota_grupo_ibfk_1` FOREIGN KEY (`CDP_ID`) REFERENCES `curso_disciplina_periodo` (`CDP_ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
