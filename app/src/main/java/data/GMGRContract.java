package data;

import android.provider.BaseColumns;

public class GMGRContract {
    public static final class CursoEntry implements BaseColumns{
        public static final String TABELA_NOME = "curso";

        public static final String _ID = "CUR_ID";
        public static final String COLUNA_DESCRICAO = "CUR_DESC";
        public static final String COLUNA_INSTITUICAO = "CUR_INSTITUICAO";
        public static final String COLUNA_PERIODO_PADRAO = "CUR_PERIODO";
    }

    public static final class PeriodoMesesEntry implements BaseColumns{
        public static final String TABELA_NOME = "periodo_meses";

        public static final String _ID = "PEM_ID";
        public static final String COLUNA_DESCRICAO = "PEM_DESC";
        public static final String COLUNA_MESES = "PEM_MESES";
    }

    public static final class NotaEntry implements BaseColumns{
        public static final String TABELA_NOME = "nota";

        public static final String _ID = "NTA_ID";
        public static final String COLUNA_NOTA_GRUPO_ID = "NTG_ID";
        public static final String COLUNA_VALOR = "NTA_VALOR";
        public static final String COLUNA_DESCRICAO = "NTA_DESC";
        public static final String COLUNA_DATA = "NTA_DATA";
    }

    public static final class DisciplinaEntry implements BaseColumns{
        public static final String TABELA_NOME = "disciplina";

        public static final String _ID = "DIS_ID";
        public static final String COLUNA_NOTA_GRUPO_ID = "DIS_DESC";
        public static final String COLUNA_VALOR = "NTA_VALOR";
        public static final String COLUNA_DESCRICAO = "NTA_DESC";
        public static final String COLUNA_DATA = "NTA_DATA";
    }

    public static final class CursoDisciplinaPeriodoEntry implements BaseColumns{
        public static final String TABELA_NOME = "curso_disciplina_periodo";

        public static final String _ID = "CDP_ID";
        public static final String COLUNA_CURSO_ID = "CUR_ID";
        public static final String COLUNA_DISCIPLINA_ID = "DIS_ID";
        public static final String COLUNA_PERIODO_MESES_ID = "PEM_ID";
        public static final String COLUNA_DATA_INICIO = "CDP_DATA_INICIO";
    }

    public static final class NotaGrupoEntry implements BaseColumns{
        public static final String TABELA_NOME = "nota_grupo";

        public static final String _ID = "NTG_ID";
        public static final String COLUNA_CURSO_DISCIPLINA_PERIODO_ID = "CDP_ID";
        public static final String COLUNA_PESO = "NTG_PESO";
        public static final String COLUNA_MEDIA_ARITMETICA = "NTG_MEDIA_ARITMETICA";
    }

    public static final class PeriodoLetivoEntry implements BaseColumns{
        public static final String TABELA_NOME = "periodo_letivo";

        public static final String _ID = "PEL_ID";
        public static final String COLUNA_DATA_INICIO = "PEL_DATA_INICIO";
        public static final String COLUNA_DATA_FIM = "PEL_DATA_FIM";
    }
}
