package model;

import java.io.Serializable;

public class Curso implements Serializable {
    private int id;
    private String descricao;
    private String instituicao;
    private int periodoPadrao;

    public Curso(int id, String descricao, String instituicao, int periodoPadrao) {
        this.id = id;
        this.descricao = descricao;
        this.instituicao = instituicao;
        this.periodoPadrao = periodoPadrao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getInstituicao() {
        return instituicao;
    }

    public void setInstituicao(String instituicao) {
        this.instituicao = instituicao;
    }

    public int getPeriodoPadrao() {
        return periodoPadrao;
    }

    public void setPeriodoPadrao(int periodoPadrao) {
        this.periodoPadrao = periodoPadrao;
    }
}
