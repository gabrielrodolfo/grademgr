package model;

import java.io.Serializable;

public class PeriodoMeses implements Serializable {
    private int periodoId;
    private String descricao;
    private int periodoMeses;

    public int getPeriodoId() {
        return periodoId;
    }

    public void setPeriodoId(int periodoId) {
        this.periodoId = periodoId;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getPeriodoMeses() {
        return periodoMeses;
    }

    public void setPeriodoMeses(int periodoMeses) {
        this.periodoMeses = periodoMeses;
    }

    public PeriodoMeses(int periodoId, String descricao, int periodoMeses) {
        this.periodoId = periodoId;
        this.descricao = descricao;
        this.periodoMeses = periodoMeses;
    }
}
