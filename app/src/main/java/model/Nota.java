package model;

import java.io.Serializable;
import java.util.Date;

public class Nota implements Serializable {
    private int id;
    private int idGrupo;
    private double valor;
    private String descricao;
    private Date data;

    public Nota(int id, int idGrupo, double valor, String descricao, Date data) {
        this.id = id;
        this.idGrupo = idGrupo;
        this.valor = valor;
        this.descricao = descricao;
        this.data = data;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(int idGrupo) {
        this.idGrupo = idGrupo;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }
}
