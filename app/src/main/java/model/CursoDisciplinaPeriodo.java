package model;

import java.io.Serializable;
import java.sql.Date;

public class CursoDisciplinaPeriodo implements Serializable {
    private int id;
    private int idCurso;
    private int idDisciplina;
    private int idPeriodoMeses;
    private Date dataInicio;

    public CursoDisciplinaPeriodo(int id, int idCurso, int idDisciplina, int idPeriodoMeses, Date dataInicio) {
        this.id = id;
        this.idCurso = idCurso;
        this.idDisciplina = idDisciplina;
        this.idPeriodoMeses = idPeriodoMeses;
        this.dataInicio = dataInicio;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdCurso() {
        return idCurso;
    }

    public void setIdCurso(int idCurso) {
        this.idCurso = idCurso;
    }

    public int getIdDisciplina() {
        return idDisciplina;
    }

    public void setIdDisciplina(int idDisciplina) {
        this.idDisciplina = idDisciplina;
    }

    public int getIdPeriodoMeses() {
        return idPeriodoMeses;
    }

    public void setIdPeriodoMeses(int idPeriodoMeses) {
        this.idPeriodoMeses = idPeriodoMeses;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }
}
