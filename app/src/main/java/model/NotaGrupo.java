package model;

import java.io.Serializable;

public class NotaGrupo implements Serializable {
    private int id;
    private int idDiscipinaPeriodo;
    private double peso;
    private double mediaAritimetica;

    public NotaGrupo(int id, int idDiscipinaPeriodo, double peso, double mediaAritimetica) {
        this.id = id;
        this.idDiscipinaPeriodo = idDiscipinaPeriodo;
        this.peso = peso;
        this.mediaAritimetica = mediaAritimetica;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdDiscipinaPeriodo() {
        return idDiscipinaPeriodo;
    }

    public void setIdDiscipinaPeriodo(int idDiscipinaPeriodo) {
        this.idDiscipinaPeriodo = idDiscipinaPeriodo;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public double getMediaAritimetica() {
        return mediaAritimetica;
    }

    public void setMediaAritimetica(double mediaAritimetica) {
        this.mediaAritimetica = mediaAritimetica;
    }
}
