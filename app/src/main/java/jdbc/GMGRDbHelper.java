package jdbc;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import data.GMGRContract.*;
import model.CursoDisciplinaPeriodo;
import model.Nota;
import model.PeriodoLetivo;

import static android.content.ContentValues.TAG;

public class GMGRDbHelper extends SQLiteOpenHelper {
    private static final String NOME_BANCO = "GradeMGR.db";
    private static final int VERSAO = 1;

    private Context context;

    public GMGRDbHelper(Context context){
        super(context, NOME_BANCO, null, VERSAO);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String SQL_CREATE_TABLE_CURSO = "CREATE TABLE " +
                CursoEntry.TABELA_NOME + " (" +
                CursoEntry._ID + " INTEGER PRIMARY KEY, " +
                CursoEntry.COLUNA_DESCRICAO + " TEXT NOT NULL, " +
                CursoEntry.COLUNA_INSTITUICAO + " TEXT, " +
                CursoEntry.COLUNA_PERIODO_PADRAO + " INTEGER NOT NULL );";

        final String SQL_CREATE_TABLE_CURSO_DISCIPLINA_PERIODO = "CREATE TABLE " +
                CursoDisciplinaPeriodoEntry.TABELA_NOME + " (" +
                CursoDisciplinaPeriodoEntry._ID + " INTEGER PRIMARY KEY, " +
                CursoDisciplinaPeriodoEntry.COLUNA_CURSO_ID + " INT NOT NULL, " +
                CursoDisciplinaPeriodoEntry.COLUNA_DISCIPLINA_ID + " INT NOT NULL, " +
                CursoDisciplinaPeriodoEntry.COLUNA_PERIODO_MESES_ID + " INT NOT NULL, " +
                CursoDisciplinaPeriodoEntry.COLUNA_DATA_INICIO + " DATE NOT NULL );";

        final String SQL_CREATE_TABLE_DISCIPLINA = "CREATE TABLE " +
                DisciplinaEntry.TABELA_NOME + " (" +
                DisciplinaEntry._ID + " INTEGER PRIMARY KEY, " +
                DisciplinaEntry.COLUNA_DESCRICAO + " TEXT NOT NULL);";

        final String SQL_CREATE_TABLE_NOTA = "CREATE TABLE " +
                NotaEntry.TABELA_NOME + " (" +
                NotaEntry._ID + " INTEGER PRIMARY KEY, " +
                NotaEntry.COLUNA_NOTA_GRUPO_ID + " INT NOT NULL, " +
                NotaEntry.COLUNA_VALOR + " DOUBLE NOT NULL, " +
                NotaEntry.COLUNA_DESCRICAO + " TEXT NOT NULL, " +
                NotaEntry.COLUNA_DATA + " DATE NOT NULL );";

        final String SQL_CREATE_TABLE_NOTA_GRUPO = "CREATE TABLE " +
                NotaGrupoEntry.TABELA_NOME + " (" +
                NotaGrupoEntry._ID + " INTEGER PRIMARY KEY, " +
                NotaGrupoEntry.COLUNA_CURSO_DISCIPLINA_PERIODO_ID + " INT NOT NULL, " +
                NotaGrupoEntry.COLUNA_PESO + " DOUBLE NOT NULL DEFAULT 100, " +
                NotaGrupoEntry.COLUNA_MEDIA_ARITMETICA + " DOUBLE NOT NULL DEFAULT 0);";

        final String SQL_CREATE_TABLE_PERIODO_LETIVO = "CREATE TABLE " +
                PeriodoLetivoEntry.TABELA_NOME + " (" +
                PeriodoLetivoEntry._ID + " INTEGER PRIMARY KEY, " +
                PeriodoLetivoEntry.COLUNA_DATA_INICIO + " DATE NOT NULL, " +
                PeriodoLetivoEntry.COLUNA_DATA_FIM + " DATE NOT NULL );";

        final String SQL_CREATE_TABLE_PERIODO_MESES = "CREATE TABLE " +
                PeriodoMesesEntry.TABELA_NOME + " (" +
                PeriodoMesesEntry._ID + " INTEGER PRIMARY KEY, " +
                PeriodoMesesEntry.COLUNA_DESCRICAO + " TEXT NOT NULL, " +
                PeriodoMesesEntry.COLUNA_MESES + " INTEGER NOT NULL );";

        db.execSQL(SQL_CREATE_TABLE_CURSO);
        db.execSQL(SQL_CREATE_TABLE_CURSO_DISCIPLINA_PERIODO);
        db.execSQL(SQL_CREATE_TABLE_DISCIPLINA);
        db.execSQL(SQL_CREATE_TABLE_NOTA);
        db.execSQL(SQL_CREATE_TABLE_NOTA_GRUPO);
        db.execSQL(SQL_CREATE_TABLE_PERIODO_LETIVO);
        db.execSQL(SQL_CREATE_TABLE_PERIODO_MESES);

        Log.i(TAG, "onCreate: Tabelas Criadas");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        final String[] TABELAS = new String[] {
                CursoEntry.TABELA_NOME,
                CursoDisciplinaPeriodoEntry.TABELA_NOME,
                DisciplinaEntry.TABELA_NOME,
                NotaEntry.TABELA_NOME,
                NotaGrupoEntry.TABELA_NOME,
                PeriodoLetivoEntry.TABELA_NOME,
                PeriodoMesesEntry.TABELA_NOME
        };

        String sqlDropTabelas = "";

        for (String tabela:TABELAS) {
            sqlDropTabelas += "DROP TABLE " + tabela + "; ";
        }

        db.execSQL(sqlDropTabelas);

        Log.i(TAG, "onUpgrade: RIP Tabelas");

        onCreate(db);
    }
}
