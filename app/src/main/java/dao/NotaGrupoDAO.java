package dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import data.GMGRContract.NotaGrupoEntry;
import jdbc.GMGRDbHelper;
import model.NotaGrupo;

public class NotaGrupoDAO {

    private SQLiteDatabase database;

    public NotaGrupoDAO(Context context){
        GMGRDbHelper helper = new GMGRDbHelper(context);
        database = helper.getWritableDatabase();
    }

    public void inserir(NotaGrupo... ngrupos){

        database.beginTransaction();

        try{
            for (NotaGrupo pm : ngrupos){
                ContentValues values = new ContentValues();

                values.put(NotaGrupoEntry.COLUNA_CURSO_DISCIPLINA_PERIODO_ID, pm.getIdDiscipinaPeriodo());
                values.put(NotaGrupoEntry.COLUNA_PESO, pm.getPeso());
                values.put(NotaGrupoEntry.COLUNA_MEDIA_ARITMETICA, pm.getMediaAritimetica());

                database.insert(NotaGrupoEntry.TABELA_NOME, null, values);
            }

            database.setTransactionSuccessful();
        } finally {
            database.endTransaction();
        }

    }

    public void atualizar(NotaGrupo... ngrupos){

        database.beginTransaction();

        String whereClause = NotaGrupoEntry._ID + " = ?";
        String[] params = new String[1];

        try{
            for (NotaGrupo pm : ngrupos){
                ContentValues values = new ContentValues();

                params[0] = String.valueOf(pm.getId());
                values.put(NotaGrupoEntry.COLUNA_CURSO_DISCIPLINA_PERIODO_ID, pm.getIdDiscipinaPeriodo());
                values.put(NotaGrupoEntry.COLUNA_PESO, pm.getPeso());
                values.put(NotaGrupoEntry.COLUNA_MEDIA_ARITMETICA, pm.getMediaAritimetica());

                database.update(NotaGrupoEntry.TABELA_NOME, values, whereClause, params);
            }
            database.setTransactionSuccessful();
        } finally {
            database.endTransaction();
        }

    }

    public void excluir(NotaGrupo... ngrupos){

        database.beginTransaction();

        String whereClause = NotaGrupoEntry._ID + " = ?";
        String[] params = new String[1];

        try{
            for (NotaGrupo pm : ngrupos){
                params[0] = String.valueOf(pm.getId());

                database.delete(NotaGrupoEntry.TABELA_NOME, whereClause, params);
            }
            database.setTransactionSuccessful();
        } finally {
            database.endTransaction();
        }

    }

    public List<NotaGrupo> pesquisar(){
        List<NotaGrupo> ngrupos = new ArrayList<NotaGrupo>();

        String[] columns = new String[] { "*" };

        Cursor cur = database.query(NotaGrupoEntry.TABELA_NOME, columns, null, null, null, null, NotaGrupoEntry._ID);

        while (cur.moveToNext())
        {
            ngrupos.add(new NotaGrupo(
                    cur.getInt(0),
                    cur.getInt(1),
                    cur.getDouble(2),
                    cur.getDouble(3)
                    )
            );
        }

        return ngrupos;
    }

}
