package dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import data.GMGRContract.PeriodoLetivoEntry;
import jdbc.GMGRDbHelper;
import model.PeriodoLetivo;

public class PeriodoLetivoDAO {

    private SQLiteDatabase database;

    public PeriodoLetivoDAO(Context context){
        GMGRDbHelper helper = new GMGRDbHelper(context);
        database = helper.getWritableDatabase();
    }

    public void inserir(PeriodoLetivo... pletivos){

        database.beginTransaction();

        try{
            for (PeriodoLetivo pm : pletivos){
                ContentValues values = new ContentValues();

                values.put(PeriodoLetivoEntry.COLUNA_DATA_INICIO, pm.getDataInicio().toString());
                values.put(PeriodoLetivoEntry.COLUNA_DATA_FIM, pm.getDataFim().toString());

                database.insert(PeriodoLetivoEntry.TABELA_NOME, null, values);
            }

            database.setTransactionSuccessful();
        } finally {
            database.endTransaction();
        }

    }

    public void atualizar(PeriodoLetivo... pletivos){

        database.beginTransaction();

        String whereClause = PeriodoLetivoEntry._ID + " = ?";
        String[] params = new String[1];

        try{
            for (PeriodoLetivo pm : pletivos){
                ContentValues values = new ContentValues();

                params[0] = String.valueOf(pm.getId());
                values.put(PeriodoLetivoEntry.COLUNA_DATA_INICIO, pm.getDataInicio().toString());
                values.put(PeriodoLetivoEntry.COLUNA_DATA_FIM, pm.getDataFim().toString());

                database.update(PeriodoLetivoEntry.TABELA_NOME, values, whereClause, params);
            }
            database.setTransactionSuccessful();
        } finally {
            database.endTransaction();
        }

    }

    public void excluir(PeriodoLetivo... pletivos){

        database.beginTransaction();

        String whereClause = PeriodoLetivoEntry._ID + " = ?";
        String[] params = new String[1];

        try{
            for (PeriodoLetivo pm : pletivos){
                params[0] = String.valueOf(pm.getId());

                database.delete(PeriodoLetivoEntry.TABELA_NOME, whereClause, params);
            }
            database.setTransactionSuccessful();
        } finally {
            database.endTransaction();
        }

    }

    public List<PeriodoLetivo> pesquisar(){
        List<PeriodoLetivo> pmeses = new ArrayList<PeriodoLetivo>();

        String[] columns = new String[] { "*" };

        Cursor cur = database.query(PeriodoLetivoEntry.TABELA_NOME, columns, null, null, null, null, PeriodoLetivoEntry._ID);

        while (cur.moveToNext())
        {
            pmeses.add(new PeriodoLetivo(
                    cur.getInt(0),
                    Date.valueOf(cur.getString(1)),
                    Date.valueOf(cur.getString(2))
                    )
            );
        }

        return pmeses;
    }

}
