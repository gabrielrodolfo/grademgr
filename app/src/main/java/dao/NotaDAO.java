package dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import data.GMGRContract;
import data.GMGRContract.NotaEntry;
import jdbc.GMGRDbHelper;
import model.Nota;

public class NotaDAO {

    private SQLiteDatabase database;

    public NotaDAO(Context context){
        GMGRDbHelper helper = new GMGRDbHelper(context);
        database = helper.getWritableDatabase();
    }

    public void inserir(Nota... notas){

        database.beginTransaction();

        try{
            for (Nota pm : notas){
                ContentValues values = new ContentValues();

                values.put(NotaEntry.COLUNA_NOTA_GRUPO_ID, pm.getIdGrupo());
                values.put(NotaEntry.COLUNA_VALOR, pm.getValor());
                values.put(NotaEntry.COLUNA_DESCRICAO, pm.getDescricao());
                values.put(NotaEntry.COLUNA_DATA, pm.getData().toString());

                database.insert(NotaEntry.TABELA_NOME, null, values);
            }

            database.setTransactionSuccessful();
        } finally {
            database.endTransaction();
        }

    }

    public void atualizar(Nota... notas){

        database.beginTransaction();

        String whereClause = NotaEntry._ID + " = ?";
        String[] params = new String[1];

        try{
            for (Nota pm : notas){
                ContentValues values = new ContentValues();

                params[0] = String.valueOf(pm.getId());
                values.put(NotaEntry.COLUNA_NOTA_GRUPO_ID, pm.getIdGrupo());
                values.put(NotaEntry.COLUNA_VALOR, pm.getValor());
                values.put(NotaEntry.COLUNA_DESCRICAO, pm.getDescricao());
                values.put(NotaEntry.COLUNA_DATA, pm.getData().toString());

                database.update(NotaEntry.TABELA_NOME, values, whereClause, params);
            }
            database.setTransactionSuccessful();
        } finally {
            database.endTransaction();
        }

    }

    public void excluir(Nota... notas){

        database.beginTransaction();

        String whereClause = NotaEntry._ID + " = ?";
        String[] params = new String[1];

        try{
            for (Nota pm : notas){
                params[0] = String.valueOf(pm.getId());

                database.delete(NotaEntry.TABELA_NOME, whereClause, params);
            }
            database.setTransactionSuccessful();
        } finally {
            database.endTransaction();
        }

    }

    public List<Nota> pesquisar(){
        List<Nota> notas = new ArrayList<Nota>();

        String[] columns = new String[] { "*" };

        Cursor cur = database.query(NotaEntry.TABELA_NOME, columns, null, null, null, null, NotaEntry._ID);

        while (cur.moveToNext())
        {
            notas.add(new Nota(
                    cur.getInt(0),
                    cur.getInt(1),
                    cur.getDouble(2),
                    cur.getString(3),
                    Date.valueOf(cur.getString(4))
                    )
            );
        }

        return notas;
    }

}
