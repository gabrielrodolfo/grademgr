package dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import data.GMGRContract.PeriodoMesesEntry;
import jdbc.GMGRDbHelper;
import model.PeriodoMeses;

public class PeriodoMesesDAO {

    private SQLiteDatabase database;

    public PeriodoMesesDAO(Context context){
        GMGRDbHelper helper = new GMGRDbHelper(context);
        database = helper.getWritableDatabase();
    }

    public void inserir(PeriodoMeses... periodos){

        database.beginTransaction();

        try{
            for (PeriodoMeses pm : periodos){
                ContentValues values = new ContentValues();

                values.put(PeriodoMesesEntry.COLUNA_DESCRICAO, pm.getDescricao());
                values.put(PeriodoMesesEntry.COLUNA_MESES, pm.getPeriodoMeses());

                database.insert(PeriodoMesesEntry.TABELA_NOME, null, values);
            }

            database.setTransactionSuccessful();
        } finally {
            database.endTransaction();
        }

    }

    public void atualizar(PeriodoMeses... periodos){

        database.beginTransaction();

        String whereClause = PeriodoMesesEntry._ID + " = ?";
        String[] params = new String[1];

        try{
            for (PeriodoMeses pm : periodos){
                ContentValues values = new ContentValues();

                params[0] = String.valueOf(pm.getPeriodoId());
                values.put(PeriodoMesesEntry.COLUNA_DESCRICAO, pm.getDescricao());
                values.put(PeriodoMesesEntry.COLUNA_MESES, pm.getPeriodoMeses());

                database.update(PeriodoMesesEntry.TABELA_NOME, values, whereClause, params);
            }
            database.setTransactionSuccessful();
        } finally {
            database.endTransaction();
        }

    }

    public void excluir(PeriodoMeses... periodos){

        database.beginTransaction();

        String whereClause = PeriodoMesesEntry._ID + " = ?";
        String[] params = new String[1];

        try{
            for (PeriodoMeses pm : periodos){
                params[0] = String.valueOf(pm.getPeriodoId());

                database.delete(PeriodoMesesEntry.TABELA_NOME, whereClause, params);
            }
            database.setTransactionSuccessful();
        } finally {
            database.endTransaction();
        }

    }

    public List<PeriodoMeses> pesquisar(){
        List<PeriodoMeses> pmeses = new ArrayList<PeriodoMeses>();

        String[] columns = new String[] { "*" };

        Cursor cur = database.query(PeriodoMesesEntry.TABELA_NOME, columns, null, null, null, null, PeriodoMesesEntry._ID);

        while (cur.moveToNext())
        {
            pmeses.add(new PeriodoMeses(
                    cur.getInt(0),
                    cur.getString(1),
                    cur.getInt(2)
                    )
            );
        }

        return pmeses;
    }

}
