package dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import data.GMGRContract.CursoEntry;
import jdbc.GMGRDbHelper;
import model.Curso;

public class CursoDAO {

    private SQLiteDatabase database;

    public CursoDAO(Context context){
        GMGRDbHelper helper = new GMGRDbHelper(context);
        database = helper.getWritableDatabase();
    }

    public void inserir(Curso... cursos){

        database.beginTransaction();

        try{
            for (Curso pm : cursos){
                ContentValues values = new ContentValues();

                values.put(CursoEntry.COLUNA_DESCRICAO, pm.getDescricao());
                values.put(CursoEntry.COLUNA_INSTITUICAO, pm.getInstituicao());
                values.put(CursoEntry.COLUNA_PERIODO_PADRAO, pm.getPeriodoPadrao());

                database.insert(CursoEntry.TABELA_NOME, null, values);
            }

            database.setTransactionSuccessful();
        } finally {
            database.endTransaction();
        }

    }

    public void atualizar(Curso... cursos){

        database.beginTransaction();

        String whereClause = CursoEntry._ID + " = ?";
        String[] params = new String[1];

        try{
            for (Curso pm : cursos){
                ContentValues values = new ContentValues();

                params[0] = String.valueOf(pm.getId());
                values.put(CursoEntry.COLUNA_DESCRICAO, pm.getDescricao());
                values.put(CursoEntry.COLUNA_INSTITUICAO, pm.getInstituicao());
                values.put(CursoEntry.COLUNA_PERIODO_PADRAO, pm.getPeriodoPadrao());

                database.update(CursoEntry.TABELA_NOME, values, whereClause, params);
            }
            database.setTransactionSuccessful();
        } finally {
            database.endTransaction();
        }

    }

    public void excluir(Curso... cursos){

        database.beginTransaction();

        String whereClause = CursoEntry._ID + " = ?";
        String[] params = new String[1];

        try{
            for (Curso pm : cursos){
                params[0] = String.valueOf(pm.getId());

                database.delete(CursoEntry.TABELA_NOME, whereClause, params);
            }
            database.setTransactionSuccessful();
        } finally {
            database.endTransaction();
        }

    }

    public List<Curso> pesquisar(){
        List<Curso> cursos = new ArrayList<Curso>();

        String[] columns = new String[] { "*" };

        Cursor cur = database.query(CursoEntry.TABELA_NOME, columns, null, null, null, null, CursoEntry._ID);

        while (cur.moveToNext())
        {
            cursos.add(new Curso(
                    cur.getInt(0),
                    cur.getString(1),
                    cur.getString(2),
                    cur.getInt(3)
                    )
            );
        }

        return cursos;
    }

}
