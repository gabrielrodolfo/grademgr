package dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import data.GMGRContract.DisciplinaEntry;
import jdbc.GMGRDbHelper;
import model.Disciplina;

public class DisciplinaDAO {

    private SQLiteDatabase database;

    public DisciplinaDAO(Context context){
        GMGRDbHelper helper = new GMGRDbHelper(context);
        database = helper.getWritableDatabase();
    }

    public void inserir(Disciplina... disciplinas){

        database.beginTransaction();

        try{
            for (Disciplina pm : disciplinas){
                ContentValues values = new ContentValues();

                values.put(DisciplinaEntry.COLUNA_DESCRICAO, pm.getDescricao());

                database.insert(DisciplinaEntry.TABELA_NOME, null, values);
            }

            database.setTransactionSuccessful();
        } finally {
            database.endTransaction();
        }

    }

    public void atualizar(Disciplina... disciplinas){

        database.beginTransaction();

        String whereClause = DisciplinaEntry._ID + " = ?";
        String[] params = new String[1];

        try{
            for (Disciplina pm : disciplinas){
                ContentValues values = new ContentValues();

                params[0] = String.valueOf(pm.getId());
                values.put(DisciplinaEntry.COLUNA_DESCRICAO, pm.getDescricao());

                database.update(DisciplinaEntry.TABELA_NOME, values, whereClause, params);
            }
            database.setTransactionSuccessful();
        } finally {
            database.endTransaction();
        }

    }

    public void excluir(Disciplina... disciplinas){

        database.beginTransaction();

        String whereClause = DisciplinaEntry._ID + " = ?";
        String[] params = new String[1];

        try{
            for (Disciplina pm : disciplinas){
                params[0] = String.valueOf(pm.getId());

                database.delete(DisciplinaEntry.TABELA_NOME, whereClause, params);
            }
            database.setTransactionSuccessful();
        } finally {
            database.endTransaction();
        }

    }

    public List<Disciplina> pesquisar(){
        List<Disciplina> disciplinas = new ArrayList<Disciplina>();

        String[] columns = new String[] { "*" };

        Cursor cur = database.query(DisciplinaEntry.TABELA_NOME, columns, null, null, null, null, DisciplinaEntry._ID);

        while (cur.moveToNext())
        {
            disciplinas.add(new Disciplina(
                    cur.getInt(0),
                    cur.getString(1)
                    )
            );
        }

        return disciplinas;
    }

}
