package dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import data.GMGRContract.CursoDisciplinaPeriodoEntry;
import jdbc.GMGRDbHelper;
import model.CursoDisciplinaPeriodo;

public class CursoDisciplinaPeriodoDAO {

    private SQLiteDatabase database;

    public CursoDisciplinaPeriodoDAO(Context context){
        GMGRDbHelper helper = new GMGRDbHelper(context);
        database = helper.getWritableDatabase();
    }

    public void inserir(CursoDisciplinaPeriodo... cdps){

        database.beginTransaction();

        try{
            for (CursoDisciplinaPeriodo pm : cdps){
                ContentValues values = new ContentValues();

                values.put(CursoDisciplinaPeriodoEntry.COLUNA_CURSO_ID, pm.getIdCurso());
                values.put(CursoDisciplinaPeriodoEntry.COLUNA_DISCIPLINA_ID, pm.getIdDisciplina());
                values.put(CursoDisciplinaPeriodoEntry.COLUNA_PERIODO_MESES_ID, pm.getIdPeriodoMeses());
                values.put(CursoDisciplinaPeriodoEntry.COLUNA_DATA_INICIO, pm.getDataInicio().toString());

                database.insert(CursoDisciplinaPeriodoEntry.TABELA_NOME, null, values);
            }

            database.setTransactionSuccessful();
        } finally {
            database.endTransaction();
        }

    }

    public void atualizar(CursoDisciplinaPeriodo... cdps){

        database.beginTransaction();

        String whereClause = CursoDisciplinaPeriodoEntry._ID + " = ?";
        String[] params = new String[1];

        try{
            for (CursoDisciplinaPeriodo pm : cdps){
                ContentValues values = new ContentValues();

                params[0] = String.valueOf(pm.getId());
                values.put(CursoDisciplinaPeriodoEntry.COLUNA_CURSO_ID, pm.getIdCurso());
                values.put(CursoDisciplinaPeriodoEntry.COLUNA_DISCIPLINA_ID, pm.getIdDisciplina());
                values.put(CursoDisciplinaPeriodoEntry.COLUNA_PERIODO_MESES_ID, pm.getIdPeriodoMeses());
                values.put(CursoDisciplinaPeriodoEntry.COLUNA_DATA_INICIO, pm.getDataInicio().toString());

                database.update(CursoDisciplinaPeriodoEntry.TABELA_NOME, values, whereClause, params);
            }
            database.setTransactionSuccessful();
        } finally {
            database.endTransaction();
        }

    }

    public void excluir(CursoDisciplinaPeriodo... cdps){

        database.beginTransaction();

        String whereClause = CursoDisciplinaPeriodoEntry._ID + " = ?";
        String[] params = new String[1];

        try{
            for (CursoDisciplinaPeriodo pm : cdps){
                params[0] = String.valueOf(pm.getId());

                database.delete(CursoDisciplinaPeriodoEntry.TABELA_NOME, whereClause, params);
            }
            database.setTransactionSuccessful();
        } finally {
            database.endTransaction();
        }

    }

    public List<CursoDisciplinaPeriodo> pesquisar(){
        List<CursoDisciplinaPeriodo> cdps = new ArrayList<CursoDisciplinaPeriodo>();

        String[] columns = new String[] { "*" };

        Cursor cur = database.query(CursoDisciplinaPeriodoEntry.TABELA_NOME, columns, null, null, null, null, CursoDisciplinaPeriodoEntry._ID);

        while (cur.moveToNext())
        {
            cdps.add(new CursoDisciplinaPeriodo(
                    cur.getInt(0),
                    cur.getInt(1),
                    cur.getInt(2),
                    cur.getInt(3),
                    Date.valueOf(cur.getString(4))
                    )
            );
        }

        return cdps;
    }

}
