package com.example.grademgr;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;

import dao.CursoDAO;
import model.Curso;
import util.CursoArrayAdapter;

public class ListaCursos extends AppCompatActivity {

    ArrayList<Curso> cursos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_cursos);

        FloatingActionButton fabCadastraCurso = findViewById(R.id.fabCadastroCurso);

        fabCadastraCurso.setOnClickListener((v) -> {
            Intent i = new Intent(ListaCursos.this, CadastroCurso.class);
            startActivity(i);
        });

        CursoDAO dao = new CursoDAO(this);

        cursos = (ArrayList<Curso>)dao.pesquisar();

        CursoArrayAdapter caa = new CursoArrayAdapter(this, R.id.lstCursos, cursos);

        ListView lvCursos = findViewById(R.id.lstCursos);

        lvCursos.setAdapter(caa);
    }
}
