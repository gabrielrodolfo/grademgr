package util;

import android.arch.lifecycle.AndroidViewModel;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.grademgr.R;

import java.util.ArrayList;

import model.Curso;

public class CursoArrayAdapter extends ArrayAdapter {

    private LayoutInflater layoutInflater;

    public CursoArrayAdapter(Context context, int resource, ArrayList<Curso> cursos) {
        super(context, R.layout.curso_item, cursos);

        layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View row, ViewGroup parent) {
        ViewHolder vh;

        if (row == null){
            row = layoutInflater.inflate(R.layout.curso_item, null);

            vh = new ViewHolder();
            vh.txvNome = row.findViewById(R.id.txvNomeCurso);

            row.setTag(vh);
        } else {
            vh = (ViewHolder) row.getTag();
        }

        Curso c = (Curso)getItem(position);

        vh.txvNome.setText(c.getDescricao());

        return row;
    }

    static class ViewHolder{
        TextView txvNome;
    }
}
